
#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include "PRNG/PRNG.h"
#include "PRNG/PRNG.cuh"

#include <iostream>
#include <fstream>

#define _USE_MATH_DEFINES
#include <math.h>

#define SAMPLES			10000
#define REPS			1
#define STEPS			1000
#define BETA 0.001

#define BIN_SIZE 1
#define NUM_BINS 1000
#define MIN_BIN -500



double analyticalSolution(double beta, double epsilon, double x, double R, double L)
{
	double b = beta*(L + R)*(L + R);
	double Ca = sqrt(b / (2 * M_PI*(R*R + L*L)));
	double Cb = -b / (2 * (R*R + L*L));
	double Cc = (2 * (R - L)) / (b*(epsilon + 1) / (2 * epsilon));
	return Ca*exp(Cb*(x - Cc)*(x - Cc));
}

__device__ void takeStep(double beta, double epsilon, uint4 &rngState, double &position, double target, double R, double L, bool &eating)
{
	double o, t;
	double disp = position - target;
	if (eating)
	{
		o = -(L);
		t = R;
	}
	else
	{
		o = R;
		t = -(L);
	}

	//if switching
	if (randomNumber(rngState) < 1.0 / (1.0 + epsilon*exp(beta*abs(disp)*(abs(o + disp) - abs(t + disp)))))
	{
		//switch foods
		position += o;
		eating = !eating;
	}
	else
	{
		position += t;
	}
}

__global__ void initialize(float * betas, float * epsilons, float target, uint4 * rngStates, float * positions, bool * eating, double R, double L)
{
	unsigned int index = blockIdx.x * blockDim.x + threadIdx.x;
	if (index >= SAMPLES) return;

	double beta = betas[index];
	double epsilon = epsilons[index];
	uint4 rngState = rngStates[index];
	double analyticalStddv = sqrt(1.0 / (2.0 * BETA));
	positions[index] = randomGuassian(make_float2(target, target), make_float2(analyticalStddv, analyticalStddv), rngState).x;
	eating[index] = randomNumber(rngState) > 0.5;
	rngStates[index] = rngState;

}
__global__ void takeSteps(float * betas, float * epsilons, float target, uint4 * rngStates,float * positions, bool * eatingStates, float * scores, double R, double L)
{
	unsigned int index = blockIdx.x * blockDim.x + threadIdx.x;
	if (index >= SAMPLES) return;

	double beta = betas[index];
	double epsilon = epsilons[index];
	uint4 rngState = rngStates[index];

	//double score = 0;
	double position = positions[index];
	bool eating = eatingStates[index];

	for (int i = 0; i < STEPS; ++i)
	{
		takeStep(beta, epsilon, rngState, position, target, R, L, eating);
	}
	rngStates[index] = rngState;
	positions[index] = position;
	eatingStates[index] = eating;
	//scores[index] = score/STEPS;
}

__global__ void takeStepsWithHistogram(float * betas, float * epsilons, float target, uint4 * rngStates, float * positions, bool * eatingStates, uint64_t * histogram, float * scores, double R, double L)
{
	unsigned int index = blockIdx.x * blockDim.x + threadIdx.x;
	if (index >= SAMPLES) return;

	double beta = betas[index];
	double epsilon = epsilons[index];
	uint4 rngState = rngStates[index];

	//double score = 0;
	double position = positions[index];
	bool eating = eatingStates[index];

	for (int i = 0; i < STEPS; ++i)
	{
		takeStep(beta, epsilon, rngState, position, target, R, L, eating);
		int histIndex = __float2uint_rn((position - MIN_BIN) / BIN_SIZE);
		if (histIndex < NUM_BINS)
			atomicAdd(histogram + histIndex, 1);
		//score +=  (double)fabs(position - target);
	}
	rngStates[index] = rngState;
	positions[index] = position;
	eatingStates[index] = eating;
	//scores[index] = score/STEPS;
}

int main()
{
	fastPRNG::PRNG rng;

	uint4 * rngStates = new uint4[SAMPLES];
	float * betas = new float[SAMPLES];
	float * epsilons = new float[SAMPLES];
	uint64_t * histogram = new uint64_t[NUM_BINS];
	float * scores = new float[SAMPLES];

	uint4 * d_rngStates;
	float * d_betas;
	float * d_epsilons;
	uint64_t * d_histogram;
	float * d_scores;
	float * d_positions;
	bool * d_eatingStates;

	cudaMalloc(&d_rngStates, SAMPLES * sizeof(uint4));
	cudaMalloc(&d_betas, SAMPLES * sizeof(float));
	cudaMalloc(&d_epsilons, SAMPLES * sizeof(float));
	cudaMalloc(&d_histogram, NUM_BINS * sizeof(uint64_t));
	cudaMalloc(&d_scores, SAMPLES * sizeof(float));
	cudaMalloc(&d_positions, SAMPLES * sizeof(float));
	cudaMalloc(&d_eatingStates, SAMPLES * sizeof(bool));
	
	

	for (int i = 0; i < SAMPLES; ++i)
	{
		rng.jump();
		fastPRNG::State s = rng.getState();
		
		rngStates[i] = make_uint4(s.bits0_63>>32,s.bits0_63,s.bits64_127>>32,s.bits64_127);
		betas[i] = BETA;
		epsilons[i] = 1;

	}
	
	cudaMemcpy(d_rngStates, rngStates, SAMPLES * sizeof(uint4), cudaMemcpyHostToDevice);
	cudaMemcpy(d_betas, betas, SAMPLES * sizeof(float), cudaMemcpyHostToDevice);
	cudaMemcpy(d_epsilons, epsilons, SAMPLES * sizeof(float), cudaMemcpyHostToDevice);
	
	double L = 1;
	std::cout << "(L/R)\t (S/A mean)\t (S/A max)" << std::endl;
	for (double R = L; R <= L;R += 0.1*L)
	{
		initialize << <SAMPLES / 128 + 1, 128 >> > (d_betas, d_epsilons, 0, d_rngStates, d_positions, d_eatingStates, R, L);
		for (int k = 0; k < 20000; ++k)
		{
			takeSteps << <SAMPLES / 128 + 1, 128 >> > (d_betas, d_epsilons, 0, d_rngStates, d_positions, d_eatingStates, d_scores, R, L);
		}
		for (int rep = 0; rep < REPS; ++rep)
		{
			cudaMemset(d_histogram, 0, NUM_BINS * sizeof(uint64_t));
			for (int k = 0; k < 10000; ++k)
			{
				takeSteps << <SAMPLES / 128 + 1, 128 >> > (d_betas, d_epsilons, 0, d_rngStates, d_positions, d_eatingStates, d_scores, R, L);
			}
			for (int k = 0; k < 1000; ++k)
			{
				takeStepsWithHistogram << <SAMPLES / 128 + 1, 128 >> > (d_betas, d_epsilons, 0, d_rngStates, d_positions, d_eatingStates, d_histogram, d_scores, R, L);
			}
			cudaDeviceSynchronize();

			cudaMemcpy(histogram, d_histogram, NUM_BINS * sizeof(uint64_t), cudaMemcpyDeviceToHost);
			//cudaMemcpy(scores, d_scores, SAMPLES * sizeof(float), cudaMemcpyDeviceToHost);
			//for (int i = 0; i < SAMPLES; ++i)
			//{
			//	std::cout << "Beta = " << betas[i] << "\t Epsilon = " << epsilons[i] << "\t score = " << scores[i] << std::endl;
			//}

			//histogram stuff
			std::ofstream outfile;

			outfile.open("output.tsv");
			outfile << "x\tp(x)_a\tp(x)_s" << std::endl;
			double sumA = 0;
			double sumS = 0;
			double maxA = 0;
			double maxS = 0;
			for (int i = 0; i < NUM_BINS; ++i)
			{
				double x = MIN_BIN + i*BIN_SIZE;
				double Pa = analyticalSolution(BETA, epsilons[0], x, R, L);
				double  Ps = (double)histogram[i] / ((double)(SAMPLES)*STEPS*1000*BIN_SIZE);
				if (Pa > 1e-10 || Ps > 0)
					outfile << x << "\t" << Pa << "\t" << Ps << std::endl;
				sumA += x*Pa*BIN_SIZE;
				sumS += x*Ps*BIN_SIZE;
				if (Pa > maxA)
					maxA = Pa;
				if (Ps > maxS)
					maxS = Ps;

			}
			//calc stdv
			double stdvA = 0;
			double stdvS = 0;
			for (int i = 0; i < NUM_BINS; ++i)
			{
				double x = MIN_BIN + i*BIN_SIZE;
				double Pa = analyticalSolution(BETA, epsilons[0], x, R, L);
				double  Ps = (double)histogram[i] / ((double)(SAMPLES)*STEPS * 1000 * BIN_SIZE);
				stdvA += BIN_SIZE*Pa*(sumA - x)*(sumA - x);
				stdvS += BIN_SIZE*Ps*(sumS - x)*(sumS - x);
			}

			outfile.close();
			std::cout << R/L << "\t" << sumS / sumA << "\t" << 1.0/sqrt(stdvS/stdvA) << std::endl;
		}
	}
	//std::ofstream outfile;
	//outfile.open("tradeoff.tsv");
	//for (int i = 0; i < SAMPLES; ++i)
	//{
	//	outfile << betas[i] << "\t" << epsilons[i] << "\t" << scores[i] << std::endl;
	//}
	//outfile.close();

	int bleg;
	std::cin >> bleg;

	return 0;
}
